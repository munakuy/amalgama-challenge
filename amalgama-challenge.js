/** @format */

import _ from 'lodash';
class Unit {
  constructor({
    strengthScore,
    trainingScore,
    trainingCost,
    transformationCost,
  }) {
    this.strengthScore = strengthScore;
    this.trainingScore = trainingScore;
    this.trainingCost = trainingCost;
    this.transformationCost = transformationCost;
  }
  train() {
    this.strengthScore += this.trainingScore;
  }
  getStrengthScore() {
    return this.strengthScore;
  }
  getTrainingCost() {
    return this.trainingCost;
  }
  getTransformationCost() {
    return this.transformationCost;
  }
}

class Pikeman extends Unit {
  constructor(strengthScore) {
    super({
      strengthScore: strengthScore || 5,
      trainingScore: 3,
      trainingCost: 10,
      transformationCost: 30,
    });
    this.type = 'pikeman';
  }
  transform() {
    return new Archer(this.strengthScore);
  }
}

class Archer extends Unit {
  constructor(strengthScore) {
    super({
      strengthScore: strengthScore || 10,
      trainingScore: 7,
      trainingCost: 20,
      transformationCost: 40,
    });
    this.type = 'archer';
  }
  transform() {
    return new Knight(this.strengthScore);
  }
}

class Knight extends Unit {
  constructor(strengthScore) {
    super({
      strengthScore: strengthScore || 20,
      trainingScore: 10,
      trainingCost: 30,
      transformationCost: undefined,
    });
    this.type = 'knight';
  }
  transform() {
    throw new Error('a Knight can not be transformed');
  }
}

class Army {
  constructor(noOfPikemen, noOfArchers, noOfKnights) {
    this.units = createUnits();
    this.coins = 1000;
    this.battleResults = [];

    function createUnits() {
      return [
        Array(noOfPikemen)
          .fill(undefined)
          .map((u) => new Pikeman()),
        Array(noOfArchers)
          .fill(undefined)
          .map((u) => new Archer()),
        Array(noOfKnights)
          .fill(undefined)
          .map((u) => new Knight()),
      ].flat();
    }
  }
  /**
   * @return {Number} Return the sum of all strengh score in the units
   */
  getTotalScore() {
    return this.units.reduce(function (total, unit) {
      return (total += unit.getStrengthScore());
    }, 0);
  }
  /**
   * Train the given units.
   * @param {Unit[]} units Array of units to be trainned
   * We assume the army has enough coins to train the given units
   */
  train(units) {
    units.forEach((unit) => unit.train());
    const currentTrainingCost = units.reduce(
      (total, u) => (total += u.getTrainingCost()),
      0
    );
    this.coins -= currentTrainingCost;
  }
  /**
   * Transform the given units.
   * @param {Unit[]} units  Array of units to be transformed
   */
  transform(units) {
    const newUnits = units.map((unit) => unit.transform());
    units.forEach((old) => _.remove(this.units, (u) => u == old));
    this.units = this.units.concat(newUnits);
    const currentTransformationCost = units.reduce(
      (total, u) => (total += u.getTransformationCost()),
      0
    );
    this.coins -= currentTransformationCost;
  }

  getUnits() {
    return this.units;
  }

  getCoins() {
    return this.coins;
  }

  getBattleResults() {
    return this.battleResults;
  }

  removeUnitWithMaxScore() {
    const maxUnit = this.units.reduce(function (maxUnit, currentUnit) {
      if (currentUnit.getStrengthScore() > maxUnit.getStrengthScore()) {
        return currentUnit;
      } else {
        return maxUnit;
      }
    });
    _.remove(this.units, (u) => u == maxUnit);
  }

  /**
   * It take the post war consequences when the army has lost a battle
   * The army loose two unit with the maximun strenght score.
   * Precondition: the army has lost a battle.
   */
  looserPostWarEffects() {
    this.removeUnitWithMaxScore();
    this.removeUnitWithMaxScore();
  }
  /**
   * It take the post war consequences when the army has tied a battle
   * The army loose 100 coins
   * Precondition: the army has tied a battle.
   */
  tiePostWarEffects() {
    this.coins -= 100;
  }

  /**
   * It take the post war consequences when the army has win a battle
   * The army wins 200 coins
   * Precondition: the army has win a battle.
   */
  winnerPostWarEffects() {
    this.coins += 200;
  }

  /**
   * The army add a new battle result
   * @param {BattleResult} battleResult
   */
  addNewBattleResult(battleResult) {
    this.battleResults.push(battleResult);
  }

  /**
   *
   * @param {Army} otherArmy the army to fight against with
   */
  battleAgainst(otherArmy) {
    const scoreOtherArmy = otherArmy.getTotalScore();
    const thisScore = this.getTotalScore();

    if (scoreOtherArmy == thisScore) {
      this.tiePostWarEffects();
      otherArmy.tiePostWarEffects();
      this.addNewBattleResult(new BattleResult(otherArmy, 'tie'));
      otherArmy.addNewBattleResult(new BattleResult(this, 'tie'));
    } else {
      if (scoreOtherArmy < thisScore) {
        otherArmy.looserPostWarEffects();
        this.winnerPostWarEffects();
        this.addNewBattleResult(new BattleResult(otherArmy, 'winner'));
        otherArmy.addNewBattleResult(new BattleResult(this, 'looser'));
      } else {
        this.looserPostWarEffects();
        otherArmy.winnerPostWarEffects();
        this.addNewBattleResult(new BattleResult(otherArmy, 'looser'));
        otherArmy.addNewBattleResult(new BattleResult(this, 'winner'));
      }
    }
  }
}

class Civilization {
  constructor(name, noOfPikemen, noOfArchers, noOfKnights) {
    this.name = name;
    this.armies = [];
    this.addNewArmy(noOfPikemen, noOfArchers, noOfKnights);
  }
  addNewArmy(noOfPikemen, noOfArchers, noOfKnights) {
    this.armies.push(new Army(noOfPikemen, noOfArchers, noOfKnights));
  }
}

class BattleResult {
  constructor(opponent, result) {
    this.date = new Date();
    this.opponent = opponent;
    this.result = result;
  }
  getResult() {
    return this.result;
  }
  getopponent() {
    return this.opponent;
  }
}

function main() {
  console.log('Creating Chinese civilization...');
  let chinese = new Civilization('Chinese', 2, 25, 2);
  console.log('Creating English civilization...');
  let english = new Civilization('English', 10, 10, 10);
  console.log('Adding new army to Chinese civilization...');
  english.addNewArmy(10, 15, 8);
  let chineseArmy = chinese.armies[0];
  console.log('Chinese army: total score: ', chineseArmy.getTotalScore());
  console.log('Training units of Chinese army...');
  chineseArmy.train(chineseArmy.getUnits());
  console.log(
    'Chinese army: total score after training: ',
    chineseArmy.getTotalScore()
  );
  console.log('Chinese army: coins after training', chineseArmy.getCoins());
  console.log('Transforming units of Chinese army...');
  let unitsToTransform = _.take(chineseArmy.getUnits(), 2);
  chineseArmy.transform(unitsToTransform);
  console.log(`Chinese army: coins after transforming`, chineseArmy.getCoins());
  let englishArmy = english.armies[0];

  console.log('Chinese army battling against English army...');
  chineseArmy.battleAgainst(englishArmy);
  let englishRecord = englishArmy.getBattleResults()[0];
  let chineseRecord = chineseArmy.getBattleResults()[0];
  const chineseScoreBeforeBattle = chineseArmy.getTotalScore();
  const englishScoreBeforeBattle = englishArmy.getTotalScore();
  console.log(
    `English army is the ${englishRecord.result.toUpperCase()} with score`,
    englishScoreBeforeBattle
  );
  console.log(
    `Chinese army is the ${chineseRecord.result.toUpperCase()} with score`,
    chineseScoreBeforeBattle
  );
}

main();
