# Amalgama Challenge

- Autor: Maira Diaz Marraffini
- Lenguaje: Javascript
- [Link a plataforma de ejecución](https://codepen.io/maira1001001/pen/dyMNQKM?editors=0012)

# Modelado

El modelo se diseño de forma tal que el programador no quede atado a una estrategia en particular. De esta forma el modelado de ejercitos permitirá, por ejemplo, tomar las siguientes decisiones:

1. entrenar caballeros utilizando el 80% de las monedas
2. utilizando el 30% de las monedas, entrenar la mitad de arqueros y la mitad de piqueros

> Cómo selecciona el ejercito una `n` piqueros?

Debe iterar sobre sus unidades y filtrar aquellas unidades que son piqueros `const pikemen = this.units.filter( u => u.getType() == ‘pikeman’)` y tomar aleatoreamente 5 elementos (`lodash.sampleSize(pikemen, 5)`).

# Entrenamiento

El ejército recibe un arreglo de unidades y las entrena. Se asume que el ejército contiene las suficientes monedas para entrenar a las unidades recibidas.

```
  /**
   * Train the given units.
   * @param {Unit[]} units Array of units to be trainned
   * We assume the army has enough coins to train the given units
   */
  train(units) {}
```

# Transformación

Cuando una unidad se transforma en una nueva unidad, la nueva unidad obtiene los puntos de la antigua unidad. Esto se puede observar en el constructor de `Pikeman`, `Archer` o `Knight` que toma como parámetro `strengthScore`. Por ejemplo, cuando se instancia la unidad arquero, se le pase 50 puntos de fuerza: `new Archer(50)`.


```
  /**
   * Transform the given units.
   * @param {Unit[]} units  Array of units to be transformed*
   * We assume the army has enough coins to transform the given units and
   * the given units can be transformed
   */
  transform(units)
```

Se asume que el ejercito contiene la cantidad necesaria de monedas para transformar a las unidades y que las unidades pueden ser entrenadas

# Historial de batallas

Cuando dos ejércitos batallan, cada ejercito almacena el resultado de la batalla. Cada resultado contiene la _fecha de la batalla_, _el resultado (gano, perdio o empató)_ y su _oponente_. Este diseño permitirá calcular para cada ejército, por ejemplo:

1. obtener todos los ejercitos oponentes con los que batalló

   (iterar sobre `this.battleResults` y pedirle su oponente a cada resultado de batalla (`battleResult.getOponent()` ))

2. la cantidad de batallas ganadas

   (deberá iterar sobre `this.battleResults` y filtrar por los resultados donde ganó (`result == 'winner'`)

3. los ejércitos oponentes con los que empató

   (deberá iterar sobre `this.battleResults` y filtrar por los resultados donde ganó (`result == 'looser'` y al resultado pedirle sus oponentes)
